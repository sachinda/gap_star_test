FROM node:alpine
RUN mkdir -p /home/app
WORKDIR /home/app
COPY --chown=node package*.json ./
RUN apk add --update && npm install -g nodemon  git
RUN apk add --no-cache bash git openssh
