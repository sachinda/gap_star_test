const express = require('express')
const app = express()
const port = process.env.APP_PORT;


var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var fileUpload = require('express-fileupload');
const couch_db_create = require("./common/dbcreate");


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(fileUpload());

app.use('/imageupload', require('./routes/imageupload'));


app.listen(port, async () => {


  var dbs = [

    {
      "user_name": process.env.DB_USER,
      "password": process.env.DB_USER_PASS,
      "service_url": process.env.DB_HOST,
      "service_port": Number(process.env.DB_PORT),
      "db_name": "data_db",
    },
    {
      "user_name": process.env.DB_USER,
      "password": process.env.DB_USER_PASS,
      "service_url": process.env.DB_HOST,
      "service_port": Number(process.env.DB_PORT),
      "db_name": "_users"
    },
    {
      "user_name": process.env.DB_USER,
      "password": process.env.DB_USER_PASS,
      "service_url": process.env.DB_HOST,
      "service_port": Number(process.env.DB_PORT),
      "db_name": "_replicator"
    },
    {
      "user_name": process.env.DB_USER,
      "password": process.env.DB_USER_PASS,
      "service_url": process.env.DB_HOST,
      "service_port": Number(process.env.DB_PORT),
      "db_name": "_global_changes"
    },
  ];


  try {

    var x = await couch_db_create.createDB(dbs);
    console.log("appboot db create info", x);
  } catch (error) {
    console.log(" appboot db create error", error);
  }

  console.log(`Example app listening at http://localhost:${port}`)
})