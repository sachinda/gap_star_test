
module.exports = {
  img_9_collection: {
    size: 10485760,
    mimetypes: ['image/png', 'image/jpeg', 'image/gif', 'image/bmp', 'image/svg+xml'],
    max_docs: 9,
  },
}