const nano = require('nano')('http://' + process.env.DB_USER + ':' + process.env.DB_USER_PASS + '@' + process.env.DB_HOST + ':' + process.env.DB_PORT)

module.exports = {
  nano: nano,
  selectedDb: process.env.DB
}
