"use strict";
exports.__esModule = true;
var upload_client_1 = require("@uploadcare/upload-client");
var client = new upload_client_1["default"]({ publicKey: process.env.UPLOADCARE_PUBLIC_KEY });
module.exports = client;
