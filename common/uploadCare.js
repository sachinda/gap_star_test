const uploadcareClient = require("../config/uploadCareClient");
const axios = require('axios');

module.exports = {
  uploadImage: async (file) => {
    return new Promise(async function (resolve, reject) {

      try {
        var upload = await uploadcareClient.uploadFile(file.data);
        return resolve(upload);
      } catch (error) {
        console.log('error :>> ', error);
        return reject(error);
      }

    })
  },
  deletefileincloud: function (id) {
    //UPLOADCARE_API
    // process.env.UPLOADCARE_API
    axios.defaults.baseURL = process.env.UPLOADCARE_API;

    axios.defaults.headers.common['Authorization'] = 'Uploadcare.Simple ' + process.env.UPLOADCARE_PUBLIC_KEY + ':' + process.env.UPLOADCARE_SECRET_KEY;


    // console.log('process.env.UPLOADCARE_PUBLIC_KEY', process.env.UPLOADCARE_PUBLIC_KEY);
    // console.log('process.env.UPLOADCARE_SECRET_KEY', process.env.UPLOADCARE_SECRET_KEY)

    return new Promise(async (resolve, reject) => {
      axios.delete('/files/' + id + '/storage/')
        .then(function (response) {
          return resolve(response);
          //console.log(response);
          //console.log(response.data);
          // console.log(response.status);
          // console.log(response.statusText);
          // console.log(response.headers);
          // console.log(response.config);
        }).catch(error => {
          return reject(error);
        });

    })


  },
  imageBufferdataUpload: async (bufferData) => {
    return new Promise(async function (resolve, reject) {

      try {
        var upload = await uploadcareClient.uploadFile(bufferData);
        return resolve(upload);
      } catch (error) {
        return reject(error);
      }

    })
  },
}