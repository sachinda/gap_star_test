//const nano = require('nano')('http://' + couch_user + ':' + couch_password + '@vg_prodsupp_couchdb:5984');
const hash = require('object-hash');
const Schema = require("validate");

function generateSchema() {
  return new Promise(async (resolve, reject) => {
    var schema = new Schema({
      user_name: {
        type: String,
        required: true,
      },
      password: {
        type: String,
        required: true,
      },
      service_url: {
        type: String,
        required: true,
      },
      service_port: {
        type: Number,
        required: true
      },
      db_name: {
        type: String,
        required: true,
      },
    });
    return resolve(schema);
  });
}

function validateDbmodel(data) {
  return new Promise(async (resolve, reject) => {
    try {
      var json = JSON.parse(JSON.stringify(data));
      var schema = await generateSchema()
      var error = await schema.validate(json);

      //console.log('error ---->>>', error)
      if (error.length == 0) {
        return resolve();
      } else {
        return reject('Model Validation error');
      }
    } catch (error) {
      return reject(error);
    }
  });
}

function dbconnectionMaker(user, password, url, port) {
  return new Promise(async (resolve, reject) => {
    var nano = require('nano')('http://' + user + ':' + password + '@' + url + ':' + port);
    return resolve(nano);
  });
}

function checkDbexist(connection, db) {
  return new Promise(async (resolve, reject) => {
    connection.db.get(db).then((body) => {
      return resolve(true);
    }).catch(error => {
      if (error.statusCode != undefined && error.statusCode == 404) {
        return resolve(false);
      } else {
        return reject(error);
      }
    })
  })
}

function dbcreate(connection, db) {
  return new Promise(async (resolve, reject) => {
    connection.db.create(db).then(async function (data) {
      return resolve(data);
    }).catch(error => {
      return reject(error);
    })
  });

}

function selectDb(connection, dbname) {
  return new Promise(async (resolve, reject) => {
    var seletedDb = connection.db.use(dbname);
    return resolve(seletedDb);
  });
}

function checkDocumentExist(selectDb, id) {
  return new Promise(async (resolve, reject) => {
    selectDb.get(id).then((body) => {
      // console.log(body);
      return resolve(body)
    }).catch(error => {

      if (error.statusCode != undefined && error.statusCode == 404) {
        return resolve(false);
      } else {
        return reject(error);
      }

    });


  });


}

function insertDocument(selectDb, doc) {
  return new Promise(async (resolve, reject) => {
    selectDb
      .insert(doc)
      .then(insert => {
        return resolve(insert);
      }).catch(err => {
        return reject(err);
      });

  });


}

module.exports = {
  createDB: async function createDB(config) {
    return new Promise(async (resolve, reject) => {
      var infologs = [];
      for (var i = 0; i < config.length; i++) {
        var singleElement = config[i];

        var viewinfodoc, indexinfodoc, updatehandleinfodoc, loopinfo = {};

        try {

          try {
            await validateDbmodel(singleElement);
            loopinfo['db_name'] = singleElement.db_name
          } catch (error) {
            return reject(error);
          }

          var db = await dbconnectionMaker(singleElement.user_name, singleElement.password, singleElement.service_url, singleElement.service_port);
          try {
            var dbexist = await checkDbexist(db, singleElement.db_name);
            loopinfo['dbexist'] = true;
          } catch (error) {

            return reject(error);
          }


          if (dbexist == false) {
            loopinfo['dbexist'] = false;
            try {
              await dbcreate(db, singleElement.db_name);
              loopinfo['create db '] = true;
            } catch (error) {

              return reject(error);

              //throw error;

              // loopinfo['error'] = 'Db Create Error';
              // loopinfo['debug_info'] = error;
              // infologs.push(loopinfo);
              // continue;
            }
            // db does not exist create new one 
          }

          var select_db = await selectDb(db, singleElement.db_name);
          /*  view_document */
          if (singleElement.view_document != undefined && singleElement.view_document._id != undefined) {
            try {
              var viewDoc = await checkDocumentExist(select_db, singleElement.view_document._id);
              if (viewDoc === false) {
                try {
                  var insertViewmodel = await insertDocument(select_db, singleElement.view_document);
                  viewinfodoc = 'Document not found insert as new';
                } catch (error) {
                  return reject(error);
                  //throw error;
                  // viewinfodoc = {
                  //     'error': 'Insert View model error 1',
                  //     "debug": error
                  // };
                }
                //console.log('insertViewmodel :>> ', insertViewmodel);
              } else {
                var rev = viewDoc._rev;
                delete viewDoc._rev;
                var dbhash = hash(viewDoc);
                var viewModelhash = hash(singleElement.view_document);
                if (dbhash != viewModelhash) {
                  try {
                    singleElement.view_document._rev = rev;
                    var insertViewmodel = await insertDocument(select_db, singleElement.view_document);
                    console.log('insertViewmodel :>> ', insertViewmodel);
                    viewinfodoc = 'Document found  does not Equals to old one';
                  } catch (error) {

                    return reject(error);
                    //throw error;
                    // viewinfodoc = {
                    //     'error': 'Insert View model error 2',
                    //     "debug": error
                    // };
                  }

                } else {
                  viewinfodoc = 'Document found Equals to old one';
                }
              }
              loopinfo['view_document'] = viewinfodoc;
            } catch (error) {
              return reject(error);
              // throw error;
              // loopinfo['error'] = 'view_document check exist error';
              // loopinfo['debug_info'] = error;
            }
          }
          /* index_document   */
          if (singleElement.index_document != undefined && singleElement.index_document._id != undefined) {
            try {
              var doc = await checkDocumentExist(select_db, singleElement.index_document._id);
              if (doc === false) {
                try {
                  var insertViewmodel = await insertDocument(select_db, singleElement.index_document);
                  indexinfodoc = 'Document not found insert as new';
                  console.log('index_document :>> ', index_document);
                } catch (error) {
                  return reject(error);

                  // throw error;
                  // indexinfodoc = {
                  //     'error': 'Insert Index document error',
                  //     "debug": error
                  // };
                }
              } else {
                var rev = doc._rev;
                delete doc._rev;
                var dbhash = hash(doc);
                var Modelhash = hash(singleElement.index_document);
                if (dbhash != Modelhash) {
                  try {
                    singleElement.index_document._rev = rev;
                    var insertmodel = await insertDocument(select_db, singleElement.index_document);
                    indexinfodoc = 'Document found  does not Equals to old one';
                    console.log('insertmodel :>> ', insertmodel);
                  } catch (error) {
                    return reject(error);
                    //throw error;
                    // indexinfodoc = {
                    //     'error': 'Insert View model error',
                    //     "debug": error
                    // };
                  }
                } else {
                  indexinfodoc = 'Document found Equals to old one';
                }
              }
              loopinfo['index_document'] = indexinfodoc;
            } catch (error) {
              return reject(error);
              //throw error;
              // loopinfo['error'] = 'index_document check exist error';
              // loopinfo['debug_info'] = error;
            }
          }
          /* update handler   */
          if (singleElement.update_handler != undefined && singleElement.update_handler._id != undefined) {
            //updatehandleinfodoc
            try {
              var doc = await checkDocumentExist(select_db, singleElement.update_handler._id);
              if (doc === false) {
                try {
                  var insertmodel = await insertDocument(select_db, singleElement.update_handler);
                  console.log('index_document :>> ', insertmodel);
                  updatehandleinfodoc = 'Document not found insert as new';
                } catch (error) {

                  return reject(error);
                  //throw error;
                  // updatehandleinfodoc = {
                  //     'error': 'Insert Update handler document error',
                  //     "debug": error
                  // };
                }
              } else {
                var rev = doc._rev;
                delete doc._rev;
                var dbhash = hash(doc);
                var Modelhash = hash(singleElement.update_handler);
                if (dbhash != Modelhash) {
                  try {
                    singleElement.update_handler._rev = rev;
                    var insertmodel = await insertDocument(select_db, singleElement.update_handler);
                    console.log('insertmodel :>> ', insertmodel);
                    updatehandleinfodoc = 'Document found  does not Equals to old one';
                  } catch (error) {
                    return reject(error);
                    //throw error;
                    // updatehandleinfodoc = {
                    //     'error': 'Insert Update handler document error',
                    //     "debug": error
                    // };
                  }
                } else {
                  updatehandleinfodoc = 'Document found Equals to old one';
                }
              }
            } catch (error) {

              return reject(error);
              //throw error;
              // loopinfo['error'] = 'Update handler check exist error';
              // loopinfo['debug_info'] = error;
            }
            loopinfo['Update_handler_document'] = updatehandleinfodoc;
          }
          infologs.push(loopinfo);
        } catch (error) {
          return reject(error);
          // infologs.push({
          //     "db_name": singleElement.db_name,
          //     "error": error
          // })
          // console.log('error', error)
        }
      }
      return resolve(infologs);
    })
  }
}

