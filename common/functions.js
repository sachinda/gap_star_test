
const config = require("../config/typeconfig");
const db = require('../config/nano.js')
const selected_v2_db = db.nano.use("data_db");

module.exports = {
  sendResponse: function (res, status, message, data = false, sendText = false) {
    let senddata;
    if (data == true) {
      senddata = message;
    } else {
      senddata = {
        message: message
      };
    }
    if (!sendText) res.status(status).json(senddata);
    else res.status(status).send(senddata);
    res.end();
  },
  validateRequest: function (permission, controller, common) {
    var middleware = async function (req, res, next) {
      // add skip part
      // implemet access controlle part not implemet yet

      next();
    };
    return middleware;
  },
  basicTypeValidator: function (type) {
    return new Promise(async (resolve, reject) => {
      if (type == "" || type == null || config[type] == undefined) {
        return reject()
      } else {
        return resolve()
      }
    });
  },
  collectionValidator: function (type, req, collectionid = null) {
    return new Promise(async (resolve, reject) => {

      try {
        await module.exports.basicTypeValidator(type);
      } catch (error) {
        return reject(
          {
            "status": 400,
            "massage": "Invalied collection"
          }
        )
      }

      /* check mime type is valied */
      if (!req.files) {
        return reject(
          {
            "status": 400,
            "massage": "file not found"
          }
        )
      }
      if ((req.files != undefined && req.files.media === undefined) || !req.files.media) {
        return reject(
          {
            "status": 400,
            "massage": "media input not set"
          }
        )
      }
      //size check
      if (req.files.media.size > config[type].size) {
        return reject(
          {
            "status": 413,
            "massage": 'Document too large. Expected not more than ' + config[type].size + ' Bytes'
          }
        )
      }
      // mime check
      if (req.files.media.mimetype && !config[type].mimetypes.includes(req.files.media.mimetype)) {
        return reject(
          {
            "status": 400,
            "massage": 'mime type not supported'
          }
        )
      }
      // console.log('ddhdhd', selected_v2_db);

      if (collectionid) {
        try {
          var doc = await module.exports.getDocument(selected_v2_db, collectionid);
          if (doc.pages != undefined && typeof (doc.pages) == "object") {
            if (config[type].max_docs < (doc.pages.length) + 1) {

              return reject(
                {
                  "status": 400,
                  "massage": 'Document limit exceed'
                }
              )
            } else {
              return resolve(doc);
            }
          } else {
            return reject(
              {
                "status": 400,
                "massage": 'Selected Documet does not have pages'
              }
            )
          }

        } catch (error) {
          if (error.statusCode != undefined && error.statusCode == 404) {
            return reject(
              {
                "status": 404,
                "massage": 'Collection Document Not found '
              }
            )
          } else {
            return reject(
              {
                "status": 400,
                "massage": 'Problem in DB '
              }
            )
          }
        }
      } else {
        return resolve();
      }
    })

  },
  getDocument: function (selected_db, documentId) {
    return new Promise(async (resolve, reject) => {
      selected_db.get(documentId).then((body) => {
        return resolve(body);
      }).catch((err) => {
        return reject(err);
      });
    })
  },
  insertDocument: function (selected_db, couchdb_doc) {
    return new Promise(async (resolve, reject) => {
      selected_db.insert(couchdb_doc).then((body) => {
        return resolve(body);
      }).catch((e) => {
        return reject(e);
      });
    })
  },
  numericCheck: function (x) {
    if (typeof x == 'number' && (x === 0 || x))
      return true
    else
      return false
  },
}
