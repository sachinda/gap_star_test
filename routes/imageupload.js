const express = require('express');
const router = express.Router();
const common = require("../common/functions");
const db = require('../config/nano.js')
const arrayMove = require('array-move');
const ucFunctions = require("../common/uploadCare");
const selected_v2_db = db.nano.use("data_db");
const ucConfigs = require("../config/uploadCare");
const rp = require("request-promise");


/* add fist time collection */
router.post('/:type', common.validateRequest(), async function (req, res) {
  //const type = 'img_8_collection';
  var type = req.params.type;
  try {
    // post new collection
    await common.collectionValidator(type, req);
  } catch (error) {
    return common.sendResponse(res, error.status, error.massage);
  }
  try {
    var upload = await ucFunctions.uploadImage(req.files.media);

    var couchDoc = {
      type: type,
      pages: [{
        pageId: upload.uuid,
        active: 1,
        cdnUrl: upload.cdnUrl,
        fileType: upload.imageInfo != undefined ? upload.imageInfo.format : "svg"
      }]
    }

    var couchInsert = await selected_v2_db.insert(couchDoc);
    couchDoc._id = couchInsert.id;
    couchDoc._rev = couchInsert.rev;
    return common.sendResponse(res, 200, couchDoc, true);

  } catch (error) {
    console.log('error', error)
    return common.sendResponse(res, 500, "Image Process / Store Err.")
  }

});

/* update collection item */
router.put('/:type/:domainId?', common.validateRequest(), async function (req, res) {
  var doc;
  try {
    doc = await common.collectionValidator(req.params.type, req, req.params.domainId);
  } catch (error) {
    return common.sendResponse(res, error.status, error.massage);
  }

  try {
    var doc = await selected_v2_db.get(req.params.domainId);
    if (!doc) {
      return common.sendResponse(res, 400, "Invalid domainId");
    }

    var upload = await ucFunctions.uploadImage(req.files.media);

    var couchDoc = {
      pageId: upload.uuid,
      active: 1,
      cdnUrl: upload.cdnUrl,
      fileType: upload.imageInfo != undefined ? upload.imageInfo.format : "svg"
    }

    doc.pages.push(couchDoc);
    await selected_v2_db.insert(doc);

    common.sendResponse(res, 200, doc, true);
    return;
  } catch (error) {
    return common.sendResponse(res, 500, "Internal Server Err.");
  }
});
/* shift collection item */
router.put('/:collectionId/:from/:to', common.validateRequest(), async function (req, res) {
  const documentId = req.params.collectionId;
  const from = parseInt(req.params.from);
  const to = parseInt(req.params.to);

  if (common.numericCheck(from) && common.numericCheck(to)) {
    try {
      var doc = await common.getDocument(selected_v2_db, documentId);
      if (doc.pages == undefined || doc.pages.length == 0) {
        return common.sendResponse(res, 400, "Invalied pages");
      }
      var total_doc = doc.pages.length;

      if ((from < 0) || ((total_doc - 1) < from)) {
        return common.sendResponse(res, 400, "Invalied from number");
      }

      if ((to < 0) || ((total_doc - 1) < to)) {
        return common.sendResponse(res, 400, "Invalied to number");
      }

      var ressult = arrayMove(doc.pages, from, to)
      doc.pages = ressult;
      try {
        var resdoc = await common.insertDocument(selected_v2_db, doc);
        common.sendResponse(res, 200, doc, true);
        return;
      } catch (error) {
        console.log('db insert error', error);
        return common.sendResponse(res, 500, "Db insert error");
      }
    } catch (error) {
      if (error.statusCode != undefined && error.statusCode == 404) {
        return common.sendResponse(res, 404, "Collection Document Not found");
      } else {
        console.log('Db error', error);
        return common.sendResponse(res, 500, "Db error");
      }
    }
  }
  else {
    return common.sendResponse(res, 400, "from and :to should be numeric");
  }
});
/* get docment */
router.get('/:collectionId', common.validateRequest(), async function (req, res) {
  const documentId = req.params.collectionId;
  try {
    var doc = await common.getDocument(selected_v2_db, documentId);
    return common.sendResponse(res, 200, doc, true);
  } catch (error) {
    if (error.statusCode != undefined && error.statusCode == 404) {
      return common.sendResponse(res, 404, "Collection Document Not found");
    } else {
      console.log('Db error', error);
      return common.sendResponse(res, 500, "Db error");
    }
  }
});
/* innactive doc element */
router.patch('/:collectionId/:pageid/:bool', common.validateRequest(), async function (req, res) {

  const documentId = req.params.collectionId;
  const domainId = req.params.pageid;
  const bool = parseInt(req.params.bool);
  //console.log('bool', bool)
  if (bool === 0 || bool === 1) {

    try {
      var doc = await common.getDocument(selected_v2_db, documentId);
      if (doc.pages == undefined || doc.pages.length == 0) {
        return common.sendResponse(res, 400, "Invalied pages");
      }
      var domainfind = false;
      doc.pages.forEach(function (arrayItem) {
        if (arrayItem.pageId == domainId) {
          domainfind = true;
          arrayItem["active"] = bool
          return;
        }
      })

      if (domainfind == false) {
        return common.sendResponse(res, 400, "Requested page not found");
      }
      try {
        var resdoc = await common.insertDocument(selected_v2_db, doc);
        common.sendResponse(res, 200, doc, true);
        return;
      } catch (error) {
        console.log('db insert error', error);
        return common.sendResponse(res, 500, "Db insert error");
      }
    } catch (error) {
      if (error.statusCode != undefined && error.statusCode == 404) {
        return common.sendResponse(res, 404, "Collection Document Not found");
      } else {
        console.log('Db error', error);
        return common.sendResponse(res, 500, "Db error");
      }
    }

  }
  else {
    return common.sendResponse(res, 400, "Only boolean values 0/1 are allowed");
  }
});

/* delete collection page */
router.delete('/:collectionId/:Key', common.validateRequest(), async function (req, res) {
  var documentId = req.params.collectionId;
  var domainId = req.params.Key;

  try {
    var doc = await common.getDocument(selected_v2_db, documentId);
    if (doc.pages == undefined || doc.pages.length == 0) {
      return common.sendResponse(res, 400, "Invalied pages");
    }
    var domainfind = false;
    var newpages = [];

    for (var i = 0; i < doc.pages.length; i++) {
      var arrayItem = doc.pages[i];
      if (arrayItem.pageId == domainId) {
        try {
          var result = await ucFunctions.deletefileincloud(arrayItem.pageId);
          domainfind = true;
        } catch (error) {

          if (error && error.response != undefined && error.response.status != undefined && error.response.status == 404) {
            return common.sendResponse(res, 400, "Requested image not found in cloud");
          } else {
            console.log('error', error);
            return common.sendResponse(res, 500, "Image cloud delete error");
          }
        }
      } else {
        newpages.push(arrayItem);
      }
    }

    if (domainfind == false) {
      return common.sendResponse(res, 400, "Requested page not found");
    }

    doc.pages = newpages;
    try {
      var resdoc = await common.insertDocument(selected_v2_db, doc);
      common.sendResponse(res, 200, doc, true);
      return;
    } catch (error) {
      console.log('db insert error', error);
      return common.sendResponse(res, 500, "Db insert error");
    }
  } catch (error) {
    if (error.statusCode != undefined && error.statusCode == 404) {
      return common.sendResponse(res, 404, "Collection Document Not found");
    } else {
      console.log('Db error', error);
      return common.sendResponse(res, 500, "Db error");
    }
  }

});

router.delete("/:domainId", common.validateRequest(), async function (req, res) {
  var doc;
  try {
    doc = await selected_v2_db.get(req.params.domainId);
  } catch (error) {
    if (error.statusCode == 404) {
      return common.sendResponse(res, 422, "Unprocessable / Invalid domainId");
    }
    return common.sendResponse(res, 500, "Internal Server Err.");
  }

  try {

    var ids = [];
    doc.pages.map(el => {
      ids.push(el.pageId)
    });

    var options = {
      method: "DELETE",
      uri: `${process.env.UPLOADCARE_API}/files/storage/`,
      body: ids,
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Uploadcare.Simple ${ucConfigs.publicKey}:${ucConfigs.secretKey}`
      },
      json: true // Automatically parses the JSON string in the response
    };
    await rp(options);
    var docData = {
      collectionID: doc._id,
      collection_type: req.params.type
    };
    var del = await selected_v2_db.destroy(doc._id, doc._rev);
    common.sendResponse(res, 200, del, true);

    return;
  } catch (error) {
    console.log("DELETE /:type/:domainId ERR > > > ", error);
    return common.sendResponse(res, 500, "Image deletion Err.");
  }

});
module.exports = router;
